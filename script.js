
//Activity on ToDoList
function ToDoList(){
	var currentID=0; //Used for id making
	this.lastNode;
	var taskAct=new TaskAct();
	var form=new FormElements();
	var utility=new Utility();
	this.begin=function(){
		form.initial();
		form.inPut.value="";
		form.inPut.addEventListener("keypress",addNew);
		form.deleteAll.addEventListener("click",deleteAll);
		form.selectAll.addEventListener("click",selectAllFunction);
	};
	
	//Add new 
	var addNew=function(e){
		if(e.keyCode!=13) return false;
		var taskContent=taskAct.createTaskContent(form.inPut.value);
		//Create checkbox for the task item
		var func=taskAct.getCheckBoxClickFunction(taskContent,form, updateItemLeftNumber, updateDeleteAllButton);
	 	var checkBox=utility.createInputTagWithEvent("checkbox","item"+currentID,"itemCheckBox","click",func);
	 	//create remove button for the task item
	 	func=taskAct.getRemoveButtonClick(form.toDoListElement, form, updateItemLeftNumber);
	 	var removeBtn=utility.createInputTagWithEvent("button","Remove","item"+currentID,"click",func);
	 	var taskItem=taskAct.createNewTask(checkBox,taskContent,removeBtn,currentID,form);
		//Add task item to to do todoList
		form.toDoListElement.appendChild(taskItem);
		input.value="";
		//Update the status
		currentID++;
		form.numberItemsLeft++;
		updateItemLeftNumber();
	};
	//Update the status of item left label
	var updateItemLeftNumber=function(){
		console.log("Update");
		if(form.numberItemsLeft===0)
		{

			form.numberItemsStatus.className="hidden";
		}
		else
		{
			console.log(form.numberItemsLeft);
			form.numberItemsStatus.className="";
			form.numberItemsStatus.innerHTML=form.numberItemsLeft+" items left";
		}
	};
	//Update the status of delete all checked tasks button
	var updateDeleteAllButton=function(){
		if(form.checkedItems===0)
		{
		form.deleteAllButton.className="hidden";
		}
		else
		{
			form.deleteAllButton.className="";
			if(form.checkedItems>1)
				var text="Delete all "+form.checkedItems+" items";
			else
				var text="Delete "+form.checkedItems+" item";
			form.deleteAllButton.value=text;

		}
	};
	//Select all function
	var selectAllFunction=function(){
		var checked=true;
		if(form.selectAll.checked===false) checked=false;
		var itemCheckList=document.getElementsByClassName("itemCheckBox");
		for(var i=0; i<itemCheckList.length;i++)
		{

			if(itemCheckList[i].checked===!checked)
			{

				itemCheckList[i].checked=checked;
				if(checked==true)
				{
				form.checkedItems++;
				form.numberItemsLeft--;
				utility.addClass(itemCheckList[i].parentNode.children[1],"deleted");
				}
				else
				{
				form.checkedItems--;
				form.numberItemsLeft++;
				utility.removeClass(itemCheckList[i].parentNode.children[1],"deleted");
				}	
			}
		}


		updateDeleteAllButton();
		updateItemLeftNumber();
		form.selectAll.checked==false
	};
	//Function for delete all the tasks which are checked
	var deleteAll=function()
	{
		var itemCheckList=document.getElementsByClassName("itemCheckBox");
		for(var i=0; i<itemCheckList.length;i++)
		{

			if(itemCheckList[i].checked===true)
			{

				var item=itemCheckList[i].parentNode;
				form.toDoListElement.removeChild(item);
				i--;
				form.checkedItems--;
			}
		}
		updateDeleteAllButton();
	};
}


//Constructor for object which contain all action on task
function TaskAct(){
	var utility=new Utility();
	
	//Create a task content which is user's input
	//Return a span element contain that content
	this.createTaskContent=function(inputContent)
	{
		//Create new elements
		var span=document.createElement("span");
		var content=document.createTextNode(inputContent);
		content.contentEditable="true";
		span.appendChild(content);
		return span;
	};
	//Create a task with checkBox, content and a removeButton
	this.createNewTask=function(checkBox,content,removeButton,idNum,form)
	{
		var divElement=document.createElement("div");
		var ability=new Ability();
		//Modify div element
		divElement.className="item"+idNum;
	 	//Apply drag and drop function
	 	ability.applyDragDrop(divElement,form);
		//Add item to their item container
	 	divElement.appendChild(checkBox);
		divElement.appendChild(content);
		divElement.appendChild(removeButton);
		return divElement;
	};
	//Return the function used when click on checkbox
	this.getCheckBoxClickFunction=function(taskContent,form,updateItemLeftNumber,updateDeleteAllButton)
	{
		return function(e){
			if(e.target.checked===true)
	 		{
	 			form.numberItemsLeft--;
	 			form.checkedItems++;
	 			updateItemLeftNumber();
	 			updateDeleteAllButton();
	 			utility.addClass(taskContent,"deleted");
	 		}
	 		else
	 		{
	 			form.numberItemsLeft++;
	 			form.checkedItems--;
	 			updateItemLeftNumber();
	 			updateDeleteAllButton();
	 			utility.removeClass(taskContent,"deleted");
	 		}
		};
	};
	//Return function used when click on remove button
	this.getRemoveButtonClick=function(toDoListElement,form,updateItemLeftNumber){
		return function(e){
	 		if(e.target.parentNode.children[0].checked===false)
	 		{
	 			console.log(e.target.parentNode.children[0].checked);
	 			console.log(form.numberItemsLeft);
		 		form.numberItemsLeft--;
		 		updateItemLeftNumber();
	 		}
	 		toDoListElement.removeChild(e.target.parentNode);
		}
		
	};
}

//Constructor for object which contain information of the form
function FormElements(){
	this.toDoListElement; //To do list container
	this.numberItemsLeft=0; //Tasks which are unchecked
	this.checkedItems=0; //Tasks which are checked
	this.inPut; //Element where user type the input
	this.deleteAll;
	this.numberItemsStatus;
	this.deleteAllButton;
	this.selectAll;
	this.lastNode;
	this.initial=function(){
		this.inPut=document.getElementById("input");
		this.deleteAll=document.getElementById("deleteAll");
		this.toDoListElement=document.getElementById("todoList");
		this.numberItemsStatus=document.getElementById("numberItems");
		this.deleteAllButton=document.getElementById("deleteAll");
		this.selectAll=document.getElementById("selectAllCheckBox");
	};
}

//Constructor for drag-drop object
function Ability(){
	var utility=new Utility();
	this.applyDragDrop=function(item,form){
		item.addEventListener("mousedown", function(e){
	 		form.lastNode=e.target.parentNode;
	 		if(form.lastNode.id==="todoList") //Just get the div element
	 		{
	 			console.log(form.lastNode);
	 			form.lastNode=e.target;
	 		}
	 	});
	 	item.addEventListener("mouseup", function(e){
	 		var targetNode=e.target;
	 		if(targetNode.id==="todoList")
	 		{
	 			targetNode=e.target;	 //Just get the div element
	 		}
	 		targetNode.parentNode.insertBefore(form.lastNode,targetNode);
	 	});
	};
}
//Constructor for object which contain useful methods
function Utility(){
	//Function for creating a input element and add an event
	//Prameters: type, value, className,event,event function
	this.createInputTagWithEvent=function (type,value,className,event,eventFunc){
		var input=document.createElement("input");
		input.type=type;
		input.value=value;
		input.className=className;
		input.addEventListener(event,eventFunc);
		return input;
	};
	this.addClass=function(item,classString){
		item.classList.add(classString);
	};
	this.removeClass=function(item,classString){
		item.classList.remove(classString);
	};
}



(function main(){
	var newOne=new ToDoList();
	newOne.begin();
})();

